# Generated by Django 2.1 on 2018-07-20 20:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='issue',
            name='lat',
            field=models.DecimalField(decimal_places=6, default=0, max_digits=9),
        ),
        migrations.AddField(
            model_name='issue',
            name='lon',
            field=models.DecimalField(decimal_places=6, default=0, max_digits=9),
        ),
        migrations.AlterField(
            model_name='issue',
            name='status',
            field=models.CharField(choices=[('opened', 'opened'), ('closed', 'closed')], default='opened', max_length=20),
        ),
        migrations.AlterField(
            model_name='vote',
            name='down_vote',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='vote',
            name='up_vote',
            field=models.BooleanField(default=False),
        ),
    ]
