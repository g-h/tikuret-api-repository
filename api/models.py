from collections import defaultdict
from django.utils import timezone
from django.db import models


class Location(models.Model):
    name = models.CharField(max_length=30) #unique

    def __str__(self):
        return self.name
class User(models.Model):

    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    username = models.CharField(max_length=15)
    phone_number = models.CharField(max_length=14)
#    address = models.ForeignKey(Location, on_delete=models.CASCADE)


class Issue(models.Model):

    STATUS = (
        ('opened', 'opened'),
        ('closed', 'closed',),
    )

    title = models.CharField(max_length=30)
    category = models.CharField(max_length=30)
    description = models.TextField()
    status = models.CharField(choices=STATUS, default='opened', max_length=20)
    location = models.ForeignKey(Location,on_delete=models.CASCADE)
    lat = models.DecimalField(max_digits=9,decimal_places=6,default=0)
    lon = models.DecimalField(max_digits=9,decimal_places=6,default=0)

    posted_on = models.DateTimeField(default=timezone.now)
    posted_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def has_downvoted(self,user_id):
        if Vote.objects.filter(up_vote=True,issue_id=self.pk,user_id=user_id).exists():
            return True
        return False

    def has_upvoted(self,user_id):
        if Vote.objects.filter(down_vote=True,issue_id=self.pk,user_id=user_id).exists():
            return True
        return False
    def number_of_upvote(self):
        return len(Vote.objects.filter(up_vote=True, issue=self.pk))

    def number_of_downvote(self):
        return len(Vote.objects.filter(down_vote=True, issue=self.pk))


class Vote(models.Model):
    down_vote = models.BooleanField(default=False)
    up_vote = models.BooleanField(default=False)
    issue = models.ForeignKey(Issue, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return 'downvote -> '+ self.down_vote.__str__() +\
               ' upvote -> '+ self.up_vote.__str__() +\
               ' issue id ->'+self.issue_id.__str__() +\
               ' user id ->'+self.user_id.__str__()

class CommunityLeader(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)
    location = models.ForeignKey(Location, on_delete=models.CASCADE)

