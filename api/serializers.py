from rest_framework import serializers
from rest_framework.fields import SerializerMethodField
from api.models import User, Location, Issue


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class IssueSerializer(serializers.ModelSerializer):
    upvote = SerializerMethodField()
    downvote = SerializerMethodField()
    subcity = serializers.CharField(write_only=True)
    user_id = serializers.IntegerField(write_only=True)
    location = LocationSerializer(read_only=True)
    posted_by = UserSerializer(read_only=True)
    downvoted = SerializerMethodField()
    upvoted = SerializerMethodField()

    class Meta:
        model = Issue
        exclude = ['posted_on', 'status']

    def get_downvoted(self, obj):
        return obj.has_downvoted(self.context.get("user_id"))

    def get_upvoted(self, obj):
        return obj.has_upvoted(self.context.get("user_id"))

    def get_upvote(self, obj):
        return obj.number_of_upvote()

    def get_downvote(self, obj):
        return obj.number_of_downvote()

        #extra_kwargs = {'location_name':{'write_only': True},'user_id':{'write_only': True}}

    def create(self, validated_data):
        subcity = validated_data['subcity']
        location = Location.objects.filter(name=subcity).get()
        user = User.objects.get(pk=validated_data['user_id'])
        issue = Issue.objects.create(location=location,posted_by=user)
        issue.title = validated_data.pop('title')
        issue.category = validated_data.pop('category')
        issue.description = validated_data.pop('description')
        issue.lat = validated_data.pop('lat')
        issue.lon = validated_data.pop('lon')
        issue.save()

        return issue
