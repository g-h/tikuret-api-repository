from django.urls import path
from api.views import *

urlpatterns = [
    path('register_user', RegisterUser.as_view()),
    path('open_issue', OpenIssue.as_view()),
    path('close_issue/<str:issue_id>/<str:user_id>', close_issue),
    path('latest_issues/<str:subcity>/<str:user_id>', latest_issues),
    path('upvote_issue/<str:user_id>/<str:issue_id>', upvote_issue),
    path('downvote_issue/<str:user_id>/<str:issue_id>', downvote_issue)
]