from django.shortcuts import get_object_or_404
from rest_framework import generics
from rest_framework.decorators import api_view
from rest_framework.response import Response
from api.models import User, Issue, Vote
from api.serializers import UserSerializer, IssueSerializer


class RegisterUser(generics.CreateAPIView):
    serializer_class = UserSerializer
    model = User


class OpenIssue(generics.CreateAPIView):
    serializer_class = IssueSerializer
    model = Issue


class LatestIssues(generics.ListAPIView):
    queryset = Issue.objects.all().order_by('-posted_on')
    serializer_class = IssueSerializer
    model = Issue
#context={'user_id': request.user.id}


@api_view(['GET'])
def latest_issues(request,subcity,user_id):
    issues=Issue.objects.filter(location__name=subcity)

    serializer = IssueSerializer(issues, many=True ,context={'user_id': user_id})
    return Response(serializer.data)


@api_view(['POST'])
def upvote_issue(request, user_id, issue_id):
    user = get_object_or_404(User, pk=user_id)
    issue = get_object_or_404(Issue, pk=issue_id)

    #check
    if Vote.objects.filter(issue=issue,user=user).exists():
        vote=Vote.objects.filter(issue=issue,user=user).first()
        if not vote.up_vote or vote.down_vote:
            vote.up_vote=True
            vote.down_vote=False
            vote.save()
    else:
        vote=Vote()
        vote.issue = issue
        vote.user = user
        vote.down_vote = False
        vote.up_vote = True
        vote.save()


@api_view(['POST'])
def downvote_issue(request, user_id, issue_id):
    user = get_object_or_404(User, pk=user_id)
    issue = get_object_or_404(Issue, pk=issue_id)

    if Vote.objects.filter(issue=issue, user=user).exists():
        vote = Vote.objects.filter(issue=issue, user=user).first()

        if not vote.down_vote or vote.up_vote:
            vote.up_vote = False
            vote.down_vote = True
            vote.save()
    else:
        vote = Vote()
        vote.issue = issue
        vote.user = user
        vote.down_vote = True
        vote.save()


def close_issue(request, issue_id, user_id):
    if Issue.objects.filter(pk=issue_id, posted_by_id=user_id).exists():
        issue = Issue.objects.filter(pk=issue_id, posted_by_id=user_id).first()
        issue.status = 'closed'
        issue.save()

def get_notification():
    pass

def broadcast_message():
    pass